<?php

class hapi {

	private $token = null;
	private $base_uri;

	public function __construct($uri, $user, $pass, $system = 'import') {
		$this->base_uri = $uri;
		$this->token = $this->auth($user, $pass, $system);
	}

	public function call($uri, $method = 'GET', $payload = [], $token = true, $useArray = false, $decode_result = true) {
		
		$method = strtoupper($method);
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $this->base_uri.$uri);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$headers = [];

		if ($token) {
			$headers[] = "Authorization: Bearer ".$this->token;

		}

		if ($method == 'POST') {
			$headers[] = 'Content-Type: application/json';
			curl_setopt($ch,CURLOPT_POST, 1);
			curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($payload));
		}

		if ($headers) {
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		}
		//echo "<pre>";
		//print_r($headers);

		//execute post
		$result = curl_exec($ch);

		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		
		if ($httpcode != 200) {
			$message = 'Error';
			switch ($httpcode) {
				case 403 :
					$message = 'Access Denied';
				break;
				case 500 :
					$message = 'Internal Server Error';
				break;
			}
			/*echo '<center><h1>'.$message.'</h1></center><br />';
			//echo '<center><h1>'.$headers.'</h1></center><br />';
			echo '<center><h1>'.$httpcode.'</h1></center><br />';
			echo 'API Call: '.htmlspecialchars($this->base_uri.$uri).'<br />';
			echo 'Request Payload: '.json_encode($payload);
			echo 'Response: '.$result;*/
			//die();
			throw new Exception($result, $httpcode);
		}
			//echo '<center><h1>'.$message.'</h1></center><br />';
			//echo '<center><h1>'.$headers.'</h1></center><br />';
			//echo '<center><h1>'.$httpcode.'</h1></center><br />';
			/*echo 'API Call: '.htmlspecialchars($this->base_uri.$uri).'<br />';
			print_r($headers);*/
			//echo 'Request Payload: '.json_encode($payload);
			//echo 'Response: '.$result;
		//close connection
		curl_close($ch);
		
		if ($decode_result) {
			if ($result === false) {
				throw new Exception('HAPI Error: Curl');
			}
			$result = json_decode($result, $useArray);
		/*	echo "<pre>";
		print_r($result);
		
		die();*/
			
			if (json_last_error() != JSON_ERROR_NONE) {
				throw new Exception('HAPI Error: '.json_last_error_msg());
			}

		}
		return $result;
	}

	protected function auth($user, $password, $system = 'import') {
		$payload = ["email" => $user, "password" => $password, "system" => $system];
		
		$result = $this->call('/auth', 'POST', $payload, false);

		return $result->token;
	}
}
