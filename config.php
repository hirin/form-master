<?php

date_default_timezone_set('UTC');

class config {
	protected static $config = null;

	protected static function loadConfig() {
		if (is_null(self::$config)) {
			$config = [];


			//$config['api']['hapi']['uri'] = 'http://xena.staging/api';
			$config['api']['hapi']['uri'] = 'http://192.168.43.215/leon/hapi-master/public/';
			$config['api']['hapi']['user']['forms'] = 'formimport@paintballbookingoffice.com';
			$config['api']['hapi']['pass']['forms'] = 'Ms3ZsQGjg2adG33A3cjfDBav';
			//$config['api']['hapi']['user']['regos'] = 'registrations@paintballing.com.au';
			$config['api']['hapi']['user']['regos'] = 'nicholas@paintballing.com.au';
			$config['api']['hapi']['pass']['regos'] = 'password';

			self::$config = $config;
		}
		return self::$config;
	}

	public static function get($path = '') {
		self::loadConfig();
		$return = self::$config;
		if (empty($path)) {
			return $return;
		}

		$tiers = explode('.', $path);
		foreach ($tiers as $segment) {
			if (array_key_exists($segment, $return)) {
				$return = $return[$segment];
			}
			else {
				throw new Exception('Configuration Data Not Found ('.$path.' - '.$segment.')');
			}
		}
		return $return;
	}

	public static function set($path = '', $value) {
		self::loadConfig();
		$config = &self::$config;

		$tiers = explode('.', $path);
		foreach ($tiers as $segment) {
			$config = &$config[$segment];
		}
		$config = $value;

		return true;
	}

}
