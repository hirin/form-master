<?php

class rego_php {
  protected $api;

  protected $user_key = null;
  protected $booking_id = null;
  protected $registration_id = null;
  protected $site_id = null;

  public function __construct(hapi $hapi, string $user_key = null, int $site_id = null) {
    $this->api = $hapi;
    if ($user_key !== null) {
      $booking = $this->check_user_key($user_key);
      if (!$booking) {
        throw new Exception('Invalid key');
      }
      
      $validity = $this->get_booking_validity($booking);
      if (!$validity) {
        throw new Exception('Online access is not currently permitted for this booking', 403);
      }

      $this->user_key = $user_key;
      list($this->booking_id, $crc32, $this->registration_id) = $this->decode_user_key($this->user_key);
    }
    elseif ($site_id !== null) {
      $this->site_id = $site_id;
    }
    else {     //no user_key or site_id, the user must have manually typed the link or cleared their cache
      throw new Exception('Please use the link in the confirmation SMS or Email to access this portal', 401);
    }
  }

  public function get_registration_id() {
    return $this->registration_id;
  }

  protected function decode_user_key($user_key) {
    if (!$user_key) {
      throw new Exception('Error link missing. Please check the URL and try again', 401);
    }
    $user_key = base64_decode($user_key);
    $parts = explode('|', $user_key);
    $booking_id = base64_decode($parts[0]);
    $crc32 = (isset($parts[1]) ? $parts[1] : null);
    $registration_id = (isset($parts[2]) ? base64_decode($parts[2]) : null);
    if (!$parts || sizeof($parts) < 2 || !$booking_id || (!is_int($booking_id) && !ctype_digit($booking_id)) || strlen($crc32) != 8) {
      throw new Exception('Error decoding link. Please check the URL and try again', 401);
    }
    return [
        $booking_id,      //booking_id
        $crc32,           //crc32
        $registration_id, //registration_id
    ];
  }

  //we need to cache this as its a little overkill to do this every time...
  public function check_user_key($user_key) {
    list($booking_id, $crc32, $registration_id) = $this->decode_user_key($user_key);

    $includes = [
      'site.address',
      'site.centre',
    ];
    $arguments = [

    ];
    $columns = [
      'id',
      'customer_id',
      'status_id',
      'game_date',
      'site_id',
      'site.id',
      'site.address_id',
      'site.address.id',
      'site.address.timezone',
      'site.centre.id',
      'site.centre.site_id',
      'site.centre.is_partner',
    ];

    try {
      $call = $this->api->call("/bookings/".$booking_id."?includes=".implode(',', $includes) . "&columns=".implode(',', $columns), "GET", null, true, true);
    }
    catch (Exception $ex) {
      if ($ex->getCode() == 404) {
        throw new Exception('Could not locate booking', 404);
        return false;
      }
    }

    if ($call && $call['data']) {
      $booking = $call['data'];
      $valid_hash = $this->compare_hash($crc32, $booking['customer_id']);
      if ($valid_hash) {
        return $booking;
      }
    }
    return false;
  }

  protected function compare_hash($user_supplied_hash, $customer_id) {
    return ($user_supplied_hash == hash('crc32b', $customer_id) || $user_supplied_hash == hash('crc32', $customer_id));       //crc32 was the original choice but crc32b is much easier to implement in JS, needed in the checkin app
  }

  protected function get_booking_validity($booking) {
    if ($booking['status_id'] == 3) {   //don't allow cancelled bookings
      throw new Exception('The booking has been cancelled', 410);
      return false;
    }
    if ($booking["site"]["centre"]["is_partner"]) {     //dont allow partner sites
      throw new Exception('Registration is not available for this location'. 403);
      return false;
    }
    if ($booking['game_date']) {
      if($booking['site']['address']['timezone']){
        $timezone_1=  $booking['site']['address']['timezone'];
      } else {
        $timezone_1 ='UTC';
      }
      $timezone = new DateTimeZone($timezone_1);
      //die('here');
      $centre_current_date_time = new DateTime('now', $timezone);
      $centre_game_date_time = new DateTime($booking['game_date'], $timezone);
      //cutoff is midnight of the gameday
      $centre_login_cutoff_date_time = clone $centre_game_date_time;
      $centre_login_cutoff_date_time->modify('+1 day');
      $centre_login_cutoff_date_time->setTime(0, 0, 0);

      if ($centre_current_date_time < $centre_login_cutoff_date_time) {
        return true;
      }
      else {
        throw new Exception('Access to this booking is now closed', 410);
        return false;
      }
    }
    return false;
  }

  public function confirm_registration() {
    if (!$this->registration_id) {
      throw new Exception('No registration provided');
    }
    $call = $this->api->call('/registrations/'.$this->registration_id, 'POST', ['confirmed' => true], true, true);
    return $call['data'];
  }

  public function accept_terms_safety() {
    if (!$this->registration_id) {
      throw new Exception('No registration provided');
    }
    $call = $this->api->call('/registrations/'.$this->registration_id, 'POST', ['accept_terms' => true, 'safety_brief' => true], true, true);
    return $call['data'];
  }

  public function get_booking($strip_ids = false) {
    if (!$this->user_key) {
      throw new Exception('Invalid key');
    }
    $includes = [
      'site.address',
      'site.address.country',
      'site.address.country.currency',
      'site.site_gametypes',
      'customer.emails',
      'customer.phones',
      'customer.address',
      'customer.address.country',
      'customer.address.country.currency',
      'customer.address.country',
      'registrations.customer.emails',
      'registrations.customer.phones',
      'registrations.customer.address',
      'packages',
      //'packages.toBookingPackage',
    ];
    $arguments = [

    ];
    $columns = [
      'id',
      'pax_booked',
      'game_date',
      'group',
      'status_id',
      'gametype_id',
      'site_id',
      'site.id',
      'site.name',
      'site.address_id',
      'site.address.id',
      'site.address.timezone',
      'site.address.country_id',
      'site.site_gametypes.id',
      'site.site_gametypes.site_id',
      'site.site_gametypes.gametype_id',
      'site.site_gametypes.min_age',
      'customer_id',
      'customer.id',
      'customer.name_first',
      'customer.name_last',
      'customer.emails.id',
      'customer.emails.customer_id',
      'customer.emails.value',
      'customer.phones.id',
      'customer.phones.customer_id',
      'customer.phones.value',
      'customer.address_id',
      'registrations.booking_id',
      'registrations.customer_id',
      'registrations.customer.id',
      'registrations.confirmed',
      'registrations.safety_brief',
      'registrations.accept_terms',
      'registrations.customer.name_first',
      'registrations.customer.name_last',
      'registrations.customer.emails.id',
      'registrations.customer.emails.customer_id',
      'registrations.customer.emails.value',
      'registrations.customer.phones.id',
      'registrations.customer.phones.customer_id',
      'registrations.customer.phones.value',            
    ];

    $call = $this->api->call('/bookings/'.$this->booking_id.'?includes='.implode(',', $includes) . '&columns='.implode(',', $columns), 'GET', null, true, true);

    if ($strip_ids) {
      $call['data'] = $this->strip_booking_ids($call['data']);
    }
    return $call['data'];
  }

  public function get_bookablepackages()
  {
    if (!$this->user_key) {
      throw new Exception('Invalid key');
    }
    $includes = [
      'site.address',
      'site.address.country',
      'site.address.country.currency',
      'site.site_gametypes',
      'customer.emails',
      'customer.phones',
      'customer.address',
      'customer.address.country',
      'customer.address.country.currency',
      'customer.address.country',
      'registrations.customer.emails',
      'registrations.customer.phones',
      'registrations.customer.address',
      'packages',
      //'packages.toBookingPackage',
    ];
    $arguments = [

    ];
    $columns = [
      'id',
      'pax_booked',
      'game_date',
      'group',
      'status_id',
      'gametype_id',
      'site_id',
      'site.id',
      'site.name',
      'site.address_id',
      'site.address.id',
      'site.address.timezone',
      'site.address.country_id',
      'site.site_gametypes.id',
      'site.site_gametypes.site_id',
      'site.site_gametypes.gametype_id',
      'site.site_gametypes.min_age',
      'site.address.country.currency.id',
      'customer_id',
      'customer.id',
      'customer.name_first',
      'customer.name_last',
      'customer.emails.id',
      'customer.emails.customer_id',
      'customer.emails.value',
      'customer.phones.id',
      'customer.phones.customer_id',
      'customer.phones.value',
      'customer.address_id',
      'registrations.booking_id',
      'registrations.customer_id',
      'registrations.customer.id',
      'registrations.confirmed',
      'registrations.safety_brief',
      'registrations.accept_terms',
      'registrations.customer.name_first',
      'registrations.customer.name_last',
      'registrations.customer.emails.id',
      'registrations.customer.emails.customer_id',
      'registrations.customer.emails.value',
      'registrations.customer.phones.id',
      'registrations.customer.phones.customer_id',
      'registrations.customer.phones.value',            
    ];

    $call = $this->api->call('/bookings/'.$this->booking_id.'?includes='.implode(',', $includes) . '&columns='.implode(',', $columns), 'GET', null, true, true);

    
    return $call['data'];
  }

 
  public function get_bookablepackagesproduct()
  {
    if (!$this->user_key) {
      throw new Exception('Invalid key');
    }
    $includes = [
      'site',
      'packages',      
    ];
    $arguments = [

    ];
    $columns = [
                  
    ];

    $bookings = $this->api->call('/bookings/'.$this->booking_id.'?includes='.implode(',', $includes), 'GET', null, true, true);
    $includes = array();
    $arguments = array();
    $columns = array();
    $includes = [
      'sites',      
    ];
    $arguments = [
      'package_type_id=2',
      'limit=unlimited',
      'sites.id='.$bookings['data']['site_id']      
    ];
    $columns = [
                 
    ];
   
    $data = $this->api->call('packages?'.implode('&', $arguments).'&includes='.implode(',', $includes), 'GET', null, true, true);


    
    
    $booking_ids = array();
    $booking_id_qty = array();
    
    foreach ($bookings['data']['packages'] as $booking) {
        $booking_ids[] = $booking['package_id'];         
        $booking_id_qty[$booking['package_id']] = $booking['qty_booked'];         
        $booking_packed_id[$booking['package_id']] = $booking['id'];
        if($booking['package_type_id'] == 1){
            $data['data'][] = $booking;       
        }

    }

    foreach ($data['data'] as $key => $value) {
        
        if(in_array($value['id'],$booking_ids) || $value['package_type_id'] == 1){
            
            if($value['package_type_id'] == 1){
                $value['qty_booked'] = $value['qty_booked'];
                $value['booked_packages_id'] = $value['id'];
            } else{
                $value['qty_booked'] = $booking_id_qty[$value['id']];
                $value['booked_packages_id'] = $booking_packed_id[$value['id']];
            }
            $value['type'] = 'booked';
        } else{
            $value['qty_booked'] = 0;
            $value['type'] = 'new';
            $value['booked_packages_id'] = 0;
        }
        $packages[] = $value;
    }
        
    return $packages;
  }

    public function save_extra_players($data)
    {
       
       $arguments =[
            'code='.$data['currency']
       ];
       $currencies = $this->api->call('currencies?'.implode('&', $arguments), 'GET', null, true, true);
      
        
        $booking_payload=[
            'id'=> $this->booking_id,                        
        ];
        $booking_payload['interactions']=[
           /* 'interaction_channel_id' =>1,
            'customer_id' =>3*/
        ];
        $booking_payload['packages']= json_decode($data['package_data']) ;
        $bookings = $this->api->call('bookings/'.$this->booking_id, 'POST', $booking_payload, true, true);
        
        // Add transactions After Add or update Packages
        $payload = array();
        $date = explode('/', $data['expiry_date']);
        $payload['booking_id']=$this->booking_id;
        $payload['amount']=$data['total_amount'];
        $payload['transaction_channel_id']=1;
        $payload['currency_id']=$currencies['data'][0]['id'];
        $payload['notes_user']='test';
        //$payload['status']=1;
        
        $payload['transaction_creditcard']=[
            'name_first' => $data['name_first'],
            'name_last'  =>  $data['name_last'],
            'number'   => $data['card_number'],
            'cvv' => $data['security_code'],
            'expiry_month' => $date[0] ,
            'expiry_year' => $date[1],
            'status' => 1

        ];
        $payload['address'] = [
            'postcode' => $data['billing_address_postcode'],
            'country_id' => $data['country_id'],
            'street' => $data['billing_address_street'],
            'locality' => $data['billing_address_locality'],
        ];
        $transactions = $this->api->call('transactions', 'POST', $payload, true, true);
    }

    public function add_extra_items($data)
    {
        $arguments =[
            'code='.$data['currency']
       ];
       $currencies = $this->api->call('currencies?'.implode('&', $arguments), 'GET', null, true, true);
      
        
        $booking_payload=[
            'id'=> $this->booking_id,                        
        ];
        $booking_payload['interactions']=[
            /*'interaction_channel_id' =>1,
            'customer_id' =>3*/
        ];

        $package_data = json_decode($data['package_data']);
        foreach ($package_data as $value) {
            unset($value->site);
        }
        $new_package_data = json_decode($data['newpackage_data']);
        foreach ($new_package_data as $value) {
            unset($value->site);
        };
        foreach ($new_package_data as  $value) {
            if($value->qty_booked > 0) {
                $value->package_id = $value->id;
                unset($value->id);
                $package_data[] = $value;
            }
                    }
        /*echo "<pre>";
        print_r($package_data);
        die();*/

        $booking_payload['packages']= $package_data ;
        $bookings = $this->api->call('bookings/'.$this->booking_id, 'POST', $booking_payload, true, true);
        
        // Add transactions After Add or update Packages
        $payload = array();
        $date = explode('/', $data['expiry_date']);
        $payload['booking_id']=$this->booking_id;
        $payload['amount']=$data['total_amount'];
        $payload['transaction_channel_id']=1;
        $payload['currency_id']=$currencies['data'][0]['id'];
        $payload['notes_user']='test';
        //$payload['status']=1;
        
        $payload['transaction_creditcard']=[
            'name_first' => $data['name_first'],
            'name_last'  =>  $data['name_last'],
            'number'   => $data['card_number'],
            'cvv' => $data['security_code'],
            'expiry_month' => $date[0] ,
            'expiry_year' => $date[1],
            'status' => 1
        ];
        $payload['address'] = [
            'postcode' => $data['billing_address_postcode'],
            'country_id' => $data['country_id'],
            'street' => $data['billing_address_street'],
            'locality' => $data['billing_address_locality'],
        ];
        /*echo "<pre>";
        print_r($payload);
        die();*/
        $transactions = $this->api->call('transactions', 'POST', $payload, true, true);
    }

  protected function get_site() {
    if (!$this->site_id) {
      throw new Exception('Invalid Site');
    }

    $includes = [
      'address',
    ];

    $site = $this->api->call('/sites/'.$this->site_id.'?includes='.implode(',', $includes), 'GET', null, true, true);
    return $site['data'];
  }

  public function get_groups() {
    $site = $this->get_site();
    //get the site, so we can get the timezone, so we can make sure we query for the right day
    $timezone = null;
    if ($site && $site['address'] && $site['address']['timezone']) {
      $timezone = new DateTimeZone($site['address']['timezone']);
    }
    $now = new DateTime('now', $timezone);
    $tomorrow = clone $now;
    $tomorrow->modify('+1 day');

    $includes = [
      'bookings',
    ];

    $arguments = [
      'bookings.status_id!=3',
      'bookings.site_id='.$site['id'],
      'bookings.game_date>='.$now->format('Y-m-d'),
      'bookings.game_date<'.$tomorrow->format('Y-m-d'),
      'bookings.pax_booked>0',
      'order_by=name_last,asc',
      'limit=unlimited',
    ];

    $columns = [
      'id',
      'name_first',
      'name_last',
      'bookings.id',
      'bookings.customer_id',
      'bookings.registration_key',
      'bookings.pax_booked',
    ];

    //get the groups
    $customers = $this->api->call('/customers?'.implode('&', $arguments).'&columns='.implode(',', $columns).'&includes='.implode(',', $includes), 'GET', null, true, true);
    return $customers['data'];
  }

  public function save_registration($data) {
    if (!$this->user_key) {
      throw new Exception('Invalid Key');
    }

    $payload = [
      'booking_id' => $this->booking_id,
      'safety_brief' => (isset($data['safety_brief']) ? (bool)$data['safety_brief'] : false),
      'accept_terms' => (isset($data['accept_terms']) ? (bool)$data['accept_terms'] : false),
      'customer' => [
        'name_first' => $data['name_first'],
        'name_last' => $data['name_last'],
        'date_of_birth' => $data['date_of_birth'],
        'address' => [
          'postcode' => $data['postcode'],
          'country_id' => $data['country_id'],
        ],
        'interactions' => [
          [
            'booking_id' => $this->booking_id,      //a booking not owned by this customer
            'interaction_channel_id' => 7,          //online form
            'direction_id' => 1,                    //inbound
            'notes_user' => 'Online Registration for '.trim($data['name_first']. ' ' .$data['name_last']),
            'notes_system' => json_encode([
              'ip' => self::get_client_ip(),
              'user_agent' => $_SERVER['HTTP_USER_AGENT'],
            ]),
            'tags' => [
              2,
              //need to add more tags!
            ]
          ]
        ]
      ]
    ];

    //add mobile number if we have one
    if ($data['mobile']) {
      $payload['customer']['phones'] = [
        [
          'communication' => true,
          'customer_phone_type_id' => 1,
          'value' => $data['mobile'],
        ]
      ];
    }

    //add email address if we have one
    if ($data['email']) {
      $payload['customer']['emails'] = [
        [
          'communication' => true,
          'customer_email_type_id' => 1,
          'value' => $data['email'],
        ]
      ];
    }
    if($data['type'] &&  $data['registration_id']){
      $call = $this->api->call('/registrations/'.$data['registration_id'], 'POST', $payload, true, true);
    } else {
      $call = $this->api->call('/registrations', 'POST', $payload, true, true);
    }
    return ($call && $call['data'] ? $call['data'] : false);
  }

  public static function get_client_ip() {
    $ip = $_SERVER['REMOTE_ADDR'];
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
      $ip = $_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
      $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    return $ip;
  }

  public static function get_site_id($ip = null) {
    if ($ip === null) {
      $ip = self::get_client_ip();
    }
    $ranges = [
      '10.3.25.0/24' => 55,     //newcastle
      '10.3.2.0/24' => 3,       //appin
      '10.3.10.0/24' => 50,     //petrie
      '10.1.2.0/24' => 185,     //gerrards cross
      '10.1.3.0/24' => 87,      //cobham
      '10.1.4.0/24' => 93,      //upminster a
      '10.1.5.0/24' => 207,     //upminster b
      '10.1.6.0/24' => 88,      //hemel hempstead
    ];
    foreach ($ranges as $range => $site_id) {
      if (self::cidr_match($ip, $range)) {
        return $site_id;
      }
    }
    return null;
  }

  // This checks if an ip matches a network range, eg. (10.1.1.0/24)
  protected static function cidr_match($ip, $range) {
    list ($subnet, $bits) = explode('/', $range);
    if ($bits === null) {
        $bits = 32;
    }
    $ip = ip2long($ip);
    $subnet = ip2long($subnet);
    $mask = -1 << (32 - $bits);
    $subnet &= $mask; # nb: in case the supplied subnet wasn't correctly aligned
    return ($ip & $mask) == $subnet;
 }

  public function strip_booking_ids($booking, $parents = '') {
    foreach ($booking as $field => $value) {
      if (is_array($value)) {
        $booking[$field] = $this->strip_booking_ids($value, $parents.($parents ? '.' : '').$field);
      }
      else if ($field == 'id' || substr($field, -3) == '_id') {
        if ($field == 'gametype_id') {    //stripos($parents, 'site_gametypes') !== false &&
          //we need to keep this field (both on the booking and site_gametypes for min_age check)
        }
        elseif (stripos($parents, 'registrations.') !== false && $field == 'id' && sizeof(explode('.', $parents)) == 2) {    //stripos($parents, 'site_gametypes') !== false &&
          //we need to keep this field so we know which confirmation is the current users
        }
        elseif (stripos($parents, 'site.address') !== false && $field == 'country_id' && sizeof(explode('.', $parents)) == 2) {
          //we need to keep this field so we know which country to save the new user as
        }
        else {
          unset($booking[$field]);
        }
      }
    }
    return $booking;
  }

  public function get_qr_code() {
    if (!$this->registration_id) {
      throw new Exception('No registration provided');
    }
    return $this->api->call('/registrations/'.$this->registration_id.'/qr', 'GET', [], true, false, false);
  }
}
