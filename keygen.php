<?php

require_once('../../config.php');
require_once('../../rego.php');
require_once('../../hapi.php');

$hapi = new hapi(
  config::get('api.hapi.uri'),
  config::get('api.hapi.user.regos'),
  config::get('api.hapi.pass.regos')
);

$call = $hapi->call("/bookings/".$_GET['booking_id'], "GET", null, true, true);
$booking = $call['data'];
echo '<title>Keygen</title>';
echo '<form method="GET">';
echo '<pre>';
echo 'Booking ID: <input type="text" name="booking_id" value="'.$booking['id'].'" />'.PHP_EOL;
echo 'Registration ID (Optional): <input type="text" name="registration_id" value="'.(isset($_GET['registration_id']) ? $_GET['registration_id'] : '').'" />'.PHP_EOL;


echo '<input type="submit" value="Update" />'.PHP_EOL;

//start with the booking_id
$user_key = stripTrailingEquals(base64_encode($booking['id']));

//add the crc32
$crc32 = hash('crc32', $booking['customer_id']);
echo 'Customer ID CRC32: '. $crc32.PHP_EOL;
$user_key .= '|'.$crc32;

//append the registration_id if we have it
if (isset($_GET['registration_id'])) {
  $user_key .= '|'.stripTrailingEquals(base64_encode($_GET['registration_id']));
}

//base64 encode the result
$user_key = stripTrailingEquals(base64_encode($user_key));

echo 'User Key: '.$user_key.PHP_EOL;
echo 'Intermediary: <a target="_blank" href="http://forms.staging/regos/?user_key='.$user_key.'">Here</a>'.PHP_EOL;
echo 'Portal: <a target="_blank" href="http://regos.staging/?k='.$user_key.'">Here</a>'.PHP_EOL;
echo '</pre>';
echo '</form>';


function stripTrailingEquals($user_key) {
  while (substr($user_key, -1) == '=') {
    $user_key = substr($user_key, 0, -1);
  }
  return $user_key;
}
