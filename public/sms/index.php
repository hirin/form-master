<?php

require_once('../../config.php');
require_once('../../hapi.php');

if (isset($_SERVER['PHP_AUTH_USER'])) {
  if ($_SERVER['PHP_AUTH_USER'] == '' && $_SERVER['PHP_AUTH_PW'] == '') {
    if (isset($_GET['ORIGINATOR']) && isset($_GET['RECIPIENT']) && isset($_GET['REFERENCE']) && isset($_GET['MESSAGE_TEXT'])) {
      $api = new hapi(
        config::get('api.hapi.uri'),
        config::get('api.hapi.user.forms'),
        config::get('api.hapi.pass.forms')
      );

      $interaction_id = $_GET['REFERENCE'];
      $customer_phone_number = $_GET['ORIGINATOR'];
      $our_phone_number = $_GET['RECIPIENT'];
      $message = $_GET['MESSAGE_TEXT'];

      $reponse = $api->call('/interactions/'.$interaction_id.'?includes=tags');
      if ($response) {
        $interaction = $response->data;
        if ($interaction->booking_id) {
          $booking = [
            'callback_at' => new DateTime(),    //utc?
            'interactions' => [
              [
                'customer_id' => $interaction->customer_id,
                'booking_id' => $interaction->booking_id,
                'interaction_channel_id' => 3,
                'direction_id' => 1,
                'notes_user' => 'SMS Reply From Customer',
                'sms' => [
                  'to' => $customer_phone_number,
                  'message' => $message,
                ],
                'tags' => array_map(function($tag) {
                  return $tag->id;
                }, $interaction->tags),
              ]
            ]
          ];
          $request = $api->call('/bookings/'.$interaction->booking_id, 'POST', $booking);
        }
      }

      echo "0";
      exit;
    }
  }
}

header('WWW-Authenticate: Basic realm="SMS"');
header('HTTP/1.0 401 Unauthorized');
exit;
