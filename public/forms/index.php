<?php

require_once('../../config.php');
require_once('../../hapi.php');
require_once('../../form.php');

$data = file_get_contents('php://input');
$submissions = json_decode($data, true);

function saveCustomer($form_type_id, $form_php, $withCallback = true) {
  $withComplaint = ($form_type_id == 6);
  $withRefund = $withComplaint;
  $customer = $form_php->getCustomer(true, true, true, $withCallback, $withComplaint, $withRefund);
  try {
    $customer = $form_php->saveCustomer($customer);
  }
  catch (Exception $ex) {
    $submission = $form_php->getSubmission();
    error_log('#'.$submission['id'].': Validation Failure');
    $try_again = $form_php->handleValidationResponse($ex);

    error_log('#'.$submission['id'].': Validation Retry');
    $customer = saveCustomer($form_type_id, $form_php);
  }
  return $customer;
}

$response = [];
$hapi = new hapi(
  config::get('api.hapi.uri'),
  config::get('api.hapi.user.forms'),
  config::get('api.hapi.pass.forms')
);
foreach ($submissions as $submission) {
  try {
    $form_php = new form_php($hapi);

    $response[$submission['id']] = [
      'status' => false,
      'message' => '',
      'customer_id' => null,
      'booking_id' => null,
    ];
    $domain = parse_url($submission['source_url'], PHP_URL_HOST);
    $domain = str_ireplace('www.', '', $domain);
    $form_id = $submission['form_id'];
    $form = $form_php->getForm($domain, $form_id);
    if ($form) {
      $form_php->setForm($form);
      $form_php->setSubmission($submission);

      //set the utm_campaign tag as the "Campaign" category tag
      if (isset($submission['utm_campaign']) && $submission['utm_campaign']) {
        $form_php->setCustomTag($submission['utm_campaign'], 3);
      }
      //set the utm_medium tag as the "Channel" category tag
      if (isset($submission['utm_medium']) && $submission['utm_medium']) {
        $form_php->setCustomTag($submission['utm_medium'], 2);
        //set the utm_source tag depending on the utm_medium, ie. utm_medium = "Social Media" would set the "Social Media" category
        if (isset($submission['utm_source']) && $submission['utm_source']) {
          $source_tag_category = $form_php->getTagCategory($submission['utm_medium']);
          if ($source_tag_category) {
            $form_php->setCustomTag($submission['utm_source'], $source_tag_category['id']);
          }
        }
      }

      $callback = true;
      switch($form['form_type_id']) {
        case 6 :    //refund
          $callback = false;    //no callbacks for refunds
        case 1 :    //contact
        case 2 :    //booking
          $company_id = $form_php->getCompanyTagID();
          if (!$company_id) {
            $company_id = 1;   //unknown
          }
          $form_php->setCompanyID($company_id);
          // find or create customer

          $customer = saveCustomer($form['form_type_id'], $form_php, $callback);

          try {
            $form_php->updateFormLastSubmission($form['id']);
          }
          catch (Exception $ex) {
            error_log('Could not update the Form submission time'.PHP_EOL.$ex->getMessage());
          }

          $response[$submission['id']]['status'] = true;
          $response[$submission['id']]['customer_id'] = $customer['id'];
          if ($customer['bookings'] && sizeof($customer['bookings']) == 1) {
            $response[$submission['id']]['booking_id'] = $customer['bookings'][0]['id'];
          }
        break;
      }
    }
    else {
      $response[$submission['id']]['message'] = 'Form not configured in Xena';
      error_log('Form Not Found ' . $domain . ' ' . $form_id);
    }
  }
  catch (Exception $ex) {
    $message = json_decode($ex->getMessage(), true);
    $response[$submission['id']]['message'] = ($message && $message['message'] ? $message['message'] : $ex->getMessage());
    if ($message && isset($message['errors'])) {
      $response[$submission['id']]['errors'] = $message['errors'];
    }
    error_log('#'.$submission['id'].': Skipping'.PHP_EOL.$ex->getMessage());
  }
}

header('Content-Type: application/json');
echo json_encode($response);
