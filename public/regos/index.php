<?php

require_once('../../config.php');

require_once('../../rego.php');
require_once('../../hapi.php');
header("Access-Control-Allow-Origin: *");   //allow cross domain access, should narrow this down to https://register.deltaforcepaintball.com.au....
try {
  $hapi = new hapi(
    config::get('api.hapi.uri'),
    config::get('api.hapi.user.regos'),
    config::get('api.hapi.pass.regos')
  );


  $client_site_id = rego_php::get_site_id();     //null is the internet. An integer (matches the hapi site_id) means the client is at a paintball site, identified by it's IP range coming across our VPNs

  $user_key = (isset($_GET['user_key']) && !empty($_GET['user_key']) ? $_GET['user_key'] : null);
 /* echo "<pre>";
  print_r($hapi);
  echo "<br>";
  print_r($user_key);
  echo "<br>";
  print_r($client_site_id);*/

  $rego_php = new rego_php($hapi, $user_key, $client_site_id);

  $response = [];



  $action = (isset($_GET['action']) ? $_GET['action'] : null);
  switch ($action) {
    case 'qr' :
      if (!$user_key) {
        throw new Exception('No User Key Supplied');
      }
      header('Content-Type: image/svg+xml');
      echo $rego_php->get_qr_code();
    break;
    //hirin
    case 'bookablepackages':
      $response=$rego_php->get_bookablepackages();
      response($response, 200);
    break;
    case 'bookablepackagesproduct':
      $response=$rego_php->get_bookablepackagesproduct();
      response($response, 200);
    break;
    //hirin
    default :
      if ($user_key) {
        $save_result = false;
        $registration_id = $rego_php->get_registration_id();

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
          //added by hirin
          if($action=='extraplayers'){

            save_extra_players($_POST, $rego_php);
          }
          else if($action=='AddExtraItems'){

            add_extra_items($_POST, $rego_php);
          }
          //added by hirin
          else if ($action == 'accept') {
            save_accept_terms_safety($_POST, $rego_php);
          }
          else {
            $save_result = save_registration($_POST, $rego_php);
          }
        }
        elseif($action == 'confirm' && $registration_id) {
          try {
            $rego_php->confirm_registration();
          }
          catch (Exception $ex) {
            //do nothing for now
          }
        }
        $booking = $rego_php->get_booking(true);
        $response['data'] = $booking;
        if ($save_result) {
          $response['id'] = $save_result['id'];
        }
      }
      elseif ($client_site_id) {
        $groups = $rego_php->get_groups();
        $data = [];
        foreach ($groups as $group) {
          $data[] = [
            'name_first' => $group['name_first'],
            'name_last' => $group['name_last'],
            'registration_key' => $group['bookings'][0]['registration_key'],
            'pax_booked' => $group['bookings'][0]['pax_booked'],
          ];
        }
        $response['data'] = $data;
      }
      response($response, 200);
  }
}
catch (Exception $ex) {
  throw_error($ex);
}
function save_extra_players($data,$rego_php){
  $required_fields = [
    'name_first'=>'Please add First Name',
    'name_last'=>'Please add Last Name',
    'billing_name_first'=>'Please add billing first name',
    'billing_name_last'=>'Please add billing last name',
    'billing_address_street'=>'Please add billing Address 1',
    'billing_address_locality'=>'Please add billing Address 2',
    'billing_address_town'=>'Please add billing City',
    'billing_address_region'=>'Please add billing Region',
    'billing_address_country'=>'Please add billing Country',
    /*'billing_mobile'=>'Please add billing contact number',*/
    'billing_address_postcode'=>'Please add billing postcode',
    'card_holder_name'=>'Please add card holder name',
    'card_number'=>'Please add card number',
    'security_code'=>'Please add security code',
    'expiry_date'=>'Please add expiry date'
    
    
  ];

  try {
    $errors = [];
    foreach ($required_fields as $name => $message) {
      if (!isset($data[$name]) || empty($data[$name])) {
        $errors[$name] = $message;
      }
    }
    
    if ($errors) {
      response(['errors' => $errors], 400);
    }
   /* echo "<pre>";
    print_r($data);
    die();*/
    return $rego_php->save_extra_players($data);
  }
  catch (Exception $ex) {
    if ($ex->getCode() == 400) {    //validation error
      $message = json_decode($ex->getMessage(), true);
      //print_r($message);
      
      if (isset($message['errors']) && $message['errors']) {
        $errors = $message['errors'];
      

        $validation_keys = [
          'number' => 'card_number',
          'expiry_year'=>'expiry_date',
          'expiry_month'=>'expiry_date',
          'cvv'=>'security_code'
         
        ];

        $user_errors = [];
        foreach ($validation_keys as $hapi_key => $regos_key) {

          if (array_key_exists($hapi_key, $errors)) {
            $user_errors[$regos_key] = (is_array($errors[$hapi_key][0]) ? $errors[$hapi_key][0][0] : $errors[$hapi_key][0]);   //we only show the first error, also work around double array bug
          }
        }

        if ($user_errors) {
          response(['errors' => $user_errors], 400);
        }
        

      }
      else if(isset($message['message'])){  
          response(['message' => $message['message']] , 500);
        }
    }
    throw_error($ex);
    //throw_error($ex);
  }
  return false;

}
function add_extra_items($data,$rego_php){
  $required_fields = [
    'name_first'=>'Please add First Name',
    'name_last'=>'Please add Last Name',
    'billing_name_first'=>'Please add billing first name',
    'billing_name_last'=>'Please add billing last name',
    'billing_address_street'=>'Please add billing Address 1',
    'billing_address_locality'=>'Please add billing Address 2',
    'billing_address_town'=>'Please add billing City',
    'billing_address_region'=>'Please add billing Region',
    'billing_address_country'=>'Please add billing Country',
    /*'billing_mobile'=>'Please add billing contact number',*/
    'billing_address_postcode'=>'Please add billing postcode',
    'card_holder_name'=>'Please add card holder name',
    'card_number'=>'Please add card number',
    'security_code'=>'Please add security code',
    'expiry_date'=>'Please add expiry date'
    
    
  ];

  try {
    $errors = [];
    foreach ($required_fields as $name => $message) {
      if (!isset($data[$name]) || empty($data[$name])) {
        $errors[$name] = $message;
      }
    }
    
    if ($errors) {
      response(['errors' => $errors], 400);
    }
    return $rego_php->add_extra_items($data);
  }
  catch (Exception $ex) {
    throw_error($ex);
  }
  return false;

}

function save_registration($data, $rego_php) {
  $required_fields = [
    'name_first' => 'Please enter your first name',
    'name_last' => 'Please enter your last name',
    'email' => 'Please enter your email address',
    'mobile' => 'Please enter your mobile number',
    'date_of_birth' => 'Please enter your date of birth',
    'postcode' => 'Please enter your postcode',
  ];

  $required_accepts = [
    'safety_brief' => 'Please watch the safety brief',
    'accept_terms' => 'Please accept the Terms and Conditions',
    'accept_fitness' => 'Please accept the Terms and Conditions',
    'accept_safety' => 'Please accept the Terms and Conditions',
    'accept_communication' => 'Please accept the Terms and Conditions',
  ];

  try {
    $errors = [];

    // force date of birth if under 16 hack

    $dob = dob_to_date_object($data['date_of_birth']);

    if ($dob && $dob > new DateTime('16 years ago')) {
      $required_fields['guardian'] = "Please enter the Guardian's name";
      $required_accepts['guardian_permission'] = 'The Guardian must provide permission';
    }

    foreach ($required_accepts as $name => $message) {
      if (!isset($data[$name]) || $data[$name] != "true") {
        $errors[$name] = $message;
      }
    }

    foreach ($required_fields as $name => $message) {
      if (!isset($data[$name]) || empty($data[$name])) {
        $errors[$name] = $message;
      }
    }
    if ($errors) {
      response(['errors' => $errors], 400);
    }

    return $rego_php->save_registration($data);
  }
  catch (Exception $ex) {
    if ($ex->getCode() == 400) {    //validation error
      $message = json_decode($ex->getMessage(), true);
      if (isset($message['errors']) && $message['errors']) {
        $errors = $message['errors'];

        $validation_keys = [
          'customer.phones.0.value' => 'mobile',
          'customer.emails.0.value' => 'email',
          'customer.name_first' => 'name_first',
          'customer.name_last' => 'name_last',
          'customer.address.postcode' => 'postcode',
          'customer.date_of_birth' => 'date_of_birth',
        ];

        $user_errors = [];
        foreach ($validation_keys as $hapi_key => $regos_key) {
          if (array_key_exists($hapi_key, $errors)) {
            $user_errors[$regos_key] = (is_array($errors[$hapi_key][0]) ? $errors[$hapi_key][0][0] : $errors[$hapi_key][0]);   //we only show the first error, also work around double array bug
          }
        }
        if ($user_errors) {
          response(['errors' => $user_errors], 400);
        }
      echo "<pre>";
        print_r($user_errors);
        die('here');
      }
    }
    throw_error($ex);
  }
  return false;
}

function save_accept_terms_safety($data, $rego_php) {
  $required_accepts = [
    'safety_brief' => 'Please watch the safety brief',
    'accept_terms' => 'Please accept the Terms and Conditions',
    'accept_fitness' => 'Please accept the Terms and Conditions',
    'accept_safety' => 'Please accept the Terms and Conditions',
    'accept_communication' => 'Please accept the Terms and Conditions',
  ];

  try {
    $errors = [];
    foreach ($required_accepts as $name => $message) {
      if (!isset($data[$name]) || $data[$name] != "true") {
        $errors[$name] = $message;
      }
    }
    if ($errors) {
      response(['errors' => $errors], 400);
    }

    return $rego_php->accept_terms_safety();
  }
  catch (Exception $ex) {
    throw_error($ex);
  }
  return false;
}

function response($data = [], $code = 200) {
  header('Content-Type: application/json');   //we always return json
  $site_id = rego_php::get_site_id();     //null is the internet. An integer (matches the hapi site_id) means the client is at a paintball site, identified by it's IP range coming across our VPNs
  $data['is_internal'] = ($site_id !== null);

  http_response_code($code);  //send the appropriate http status code
  echo json_encode($data);     //we don't want to include code 400 as we use that for validation
  exit;
}

function throw_error($ex) {
  $code     = $ex->getCode();
  $message  = $ex->getMessage();
  $ip       = rego_php::get_client_ip();
  $site_id  = rego_php::get_site_id($ip);
  $user_key = (isset($_GET['user_key']) && !empty($_GET['user_key']) ? $_GET['user_key'] : null);
  error_log('IP: '.$ip.($site_id ? ' | Site: '.$site_id : '').($user_key ? ' | Key: '.$user_key : '').' | '.$message.($code ? ' ('.$code.')' : ''));     // save it to the logs so we can debug it
  

  return response(['message' => ($code > 400 && $code < 500 && $message ? $message : 'An error has occured')], 500);
}

function dob_to_date_object($dob) {
  $date_obj = DateTime::createFromFormat('Y-m-d', $dob);
  if (!$date_obj) {
    $date_obj = DateTime::createFromFormat('d/m/Y', $dob);
    if (!$date_obj) {
      $date_obj = DateTime::createFromFormat('d/m/y', $dob);
    }
  }
  return $date_obj;
}
