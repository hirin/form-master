<?php

class form_php {
  protected $api;

  protected $submission = [];
  protected $form = null;
  protected $tags = [];
  protected $company_id = null;

  protected $street_as_note = false;
  protected $phone_as_note = false;
  protected $email_as_note = false;
  protected $game_date_as_note = false;
  protected $ticket_as_note = false;
  protected $postcode_as_note = false;
  protected $try_other_gametype = false;
  protected $use_unknown_name = false;

  public function __construct(hapi $hapi) {
    $this->api = $hapi;
  }

  public function setSubmission($submission) {
    $this->submission = $submission;
  }

  public function getSubmission() {
    return $this->submission;
  }

  public function setUseUnknownName($value) {
    $this->use_unknown_name = $value;
  }

  public function getUseUnknownName() {
    return $this->use_unknown_name;
  }

  public function setPhoneAsNote($value) {
    $this->phone_as_note = $value;
  }

  public function getPhoneAsNote() {
    return $this->phone_as_note;
  }

  public function setEmailAsNote($value) {
    $this->email_as_note = $value;
  }

  public function getEmailAsNote() {
    return $this->email_as_note;
  }

  public function setStreetAsNote($value) {
    $this->street_as_note = $value;
  }

  public function getStreetAsNote() {
    return $this->street_as_note;
  }

  public function setGameDateAsNote($value) {
    $this->game_date_as_note = $value;
  }

  public function getGameDateAsNote() {
    return $this->game_date_as_note;
  }

  public function setTicketAsNote($value) {
    $this->ticket_as_note = $value;
  }

  public function getTicketAsNote() {
    return $this->ticket_as_note;
  }

  public function setPostcodeAsNote($value) {
    $this->postcode_as_note = $value;
  }

  public function getPostcodeAsNote() {
    return $this->postcode_as_note;
  }

  public function setTryOtherGametype($value) {
    $this->try_other_gametype = $value;
  }

  public function getTryOtherGametype() {
    return $this->try_other_gametype;
  }

  public function setCompanyID($value) {
    $this->company_id = $value;
  }

  public function getCompanyID() {
    return $this->company_id;
  }

  public function setForm($form) {
    $this->form = $form;
    if (isset($form['source']) && $form['source']) {
      $this->setTags($form['source']['tags']);
    }
  }

  public function setTags($tags) {
    $this->tags = $tags;
  }

  public function updateFormLastSubmission($form_id) {
    $now = new DateTime('now', new DateTimeZone('utc'));
    $call = $this->api->call('/forms/'.$form_id, 'POST', ['last_submission_at' => $now->format('Y-m-d H:i:s')], true, true);
    return $call['data'];
  }

  public function saveCustomer($customer) {
    $call = $this->api->call('/customers?includes=interactions', 'POST', $customer, true, true);
    return $call['data'];
  }

  public function saveBooking($booking) {
    $call = $this->api->call('/bookings', 'POST', $booking, true, true);
    return $call['data'];
  }

  public function getCompanyTagID() {
    foreach ($this->tags as $tag) {
      if ($tag['tag_category_id'] == 1) {
        return $tag['id'];
      }
    }
    return null;
  }

  public function getForm($domain, $form_id) {
    $forms = $this->api->call('/forms?domain.url='.$domain.'&identifier='.$form_id.'&includes=source.tags', "GET", [], true, true);
    if ($forms && $forms['data']) {
      return $forms['data'][0];
    }
    return null;
  }

  public function getCountryID($country_name) {
    $country_name = trim(strtolower($country_name));
    $countries = $this->api->call('/countries?name='.$country_name, "GET", [], true, true);
    if ($countries && $countries['data']) {
      return $countries['data'][0]['id'];
    }
    else {
      $countries = $this->api->call('/countries?code='.$country_name, "GET", [], true, true);
      if ($countries && $countries['data']) {
        return $countries['data'][0]['id'];
      }
    }
    return null;
  }

  public function getCustomer($withAddress = false, $withInteraction = false, $withBooking = false, $withCallback = false, $withComplaint = false, $withRefund = false) {
    $customer = [
      'customer_type_id' => 1,
      'marketing_sms' => (isset($this->submission['marketing_sms']) ? (int)$this->submission['marketing_sms'] : 1),
      'marketing_email' => (isset($this->submission['marketing_email']) ? (int)$this->submission['marketing_email'] : 1),
      'marketing_phone' => (isset($this->submission['marketing_phone']) ? (int)$this->submission['marketing_phone'] : 1),
    ];

    if (!$this->use_unknown_name) {
      $customer['name_first'] = (isset($this->submission['name_first']) ? $this->submission['name_first'] : '');
      $customer['name_last'] = (isset($this->submission['name_last']) ? $this->submission['name_last'] : '');
    }
    else {
      $customer['name_first'] = 'Unknown';
      $customer['name_last'] = '';
    }

    if (isset($this->submission['organisation']) && $this->submission['organisation']){
      $customer['company'] = $this->submission['organisation'];
    }

    if (!$this->email_as_note && isset($this->submission['email']) && $this->submission['email']) {
      $customer['emails'] = [];
      $customer['emails'][] = [
        'value' => $this->submission['email'],
        'communication' => 1,
        'customer_email_type_id' => 1,
      ];
    }

    if (!$this->phone_as_note) {
      if ((isset($this->submission['phone_primary']) && $this->submission['phone_primary']) || (isset($this->submission['phone_secondary']) && $this->submission['phone_secondary'])) {
        $customer['phones'] = [];
        if (isset($this->submission['phone_primary']) && $this->submission['phone_primary']) {
          $customer['phones'][] = [
            'value' => $this->submission['phone_primary'],
            'communication' => 1,
            'customer_phone_type_id' => 1,
          ];
        }
        if (isset($this->submission['phone_secondary']) && $this->submission['phone_secondary']) {
          $this->customer['phones'][] = [
            'value' => $this->submission['phone_secondary'],
            'communication' => 1,
            'customer_phone_type_id' => 2,
          ];
        }
      }
    }

    if ($withAddress) {
      $customer['address'] = $this->getAddress();
    }
    if ($withBooking) {
      $country_id = (isset($customer['address']['country_id']) ? $customer['address']['country_id'] : $this->getCountryID($this->submission['country']));
      $customer['bookings'] = [
        $this->getBooking($country_id, $withInteraction, $withCallback, $withComplaint, $withRefund)
      ];
    }
    elseif ($withInteraction) {
      $customer['interactions'] = [
        $this->getInteraction()
      ];
    }
    return $customer;
  }

  public function getSiteID($site_name) {
    if (is_numeric($site_name)) {
      return $site_name;
    }
    $site_name = trim(strtolower($site_name));
    if ($site_name) {
      $sites = $this->api->call('/sites?name='.$site_name, "GET", [], true, true);
      if ($sites && $sites['data']) {
        return $sites['data'][0]['id'];
      }
    }
    return null;
  }

  public function getTag($name, $categoryID = null) {
    $tags = $this->api->call('/tags?name='.$name. ($categoryID ? '&tag_category_id='.$categoryID : ''), 'GET', [], true, true);
    if ($tags && $tags['data']) {
      return $tags['data'][0];
    }
    return null;
  }

  public function getTagCategory($name) {
    $tag_categories = $this->api->call('/tags/categories?name='.$name, 'GET', [], true, true);
    if ($tag_categories && $tag_categories['data']) {
      return $tag_categories['data'][0];
    }
    return null;
  }

  public function getBooking($country_id = null, $withInteraction = false, $withCallback = false, $withComplaint = false, $withRefund = false) {
    $site_id = (isset($this->submission['centre']) ? $this->getSiteID($this->submission['centre']) : null);
    $booking = [
      'game_date' => (!$this->game_date_as_note && isset($this->submission['game_date']) && $this->submission['game_date'] ? $this->submission['game_date']. ' 00:00:00' : null),
      'gametype_id' => $this->getGametype(),
      'gameline_id' => (isset($this->submission['gameline_id']) && $this->submission['gameline_id'] ? $this->submission['gameline_id'] : null),
      'origin_tag_id' => 350,     //online form
      'company_id' => $this->getCompanyID(),
      'status_id' => 1,   //hack to shut up validation
      'site_id' => ($site_id ? $site_id : (!$country_id || in_array($country_id, [3, 4]) ? 1 : ($country_id == 5 ? 83 : 216))),     //1 is unknown (AU), 83 is unknown (CA), 216 is unknown (UK)
      'reason_id' => (isset($this->submission['booking_reason_id']) && $this->submission['booking_reason_id'] ? $this->submission['booking_reason_id'] : null),
      'reason' => (isset($this->submission['booking_reason']) && $this->submission['booking_reason'] ? $this->submission['booking_reason'] : null),
      'callback_at' => ($withCallback ? (new DateTime('now', new DateTimeZone('utc')))->format('Y-m-d H:i:s') : null),
    ];
    if (isset($this->submission['min_age']) && $this->submission['min_age']) {
      $booking['min_age'] = $this->submission['min_age'];
    }

    if (!$this->ticket_as_note && isset($this->submission['tracking']) && $this->submission['tracking']) {
      $tracking = $this->submission['tracking'];
      if (!is_array($tracking)) {
        $tracking = str_replace([',', ';', ':', ' and ', '/', '\\'], ' ', $tracking);
        $tracking = explode(' ', $tracking);
      }

      foreach ($tracking as $code) {
        $code = trim($code);
        if ($code) {
          $booking['tickets'][] = [
            'ticket_raw' => $code,
          ];
        }
      }
    }
    if (isset($this->submission['packages']) && $this->submission['packages']) {
      $booking['packages'] = [];
      foreach ($this->submission['packages'] as $package) {
        if (isset($package['package_id']) && $package['qty_booked']) {
          $booking['packages'][] = [
            'package_id' => $package['package_id'],
            'qty_booked' => $package['qty_booked'],
          ];
        }
      }
    }

    if ($withComplaint) {
      $booking['complaints'] = [
        $this->getComplaint($withInteraction, $withRefund)
      ];
    }
    elseif ($withInteraction) {
      $booking['interactions'] = [
        $this->getInteraction()
      ];
    }
    return $booking;
  }

  public function getComplaint($withInteraction = false, $withRefund = false) {
    $complaint = [
      'complaint_department_id' => $this->submission['complaint_department'],
      'complaint_reason_id' => $this->submission['complaint_reason'],
      'complaint_resolution_id' => ($withRefund ? 3 : null),
    ];

    if ($withInteraction) {
      $complaint['interactions'] = [
        $this->getInteraction()
      ];
    }

    if ($withRefund) {
      $complaint['refunds'] = [
        $this->getRefund()
      ];
    }
    return $complaint;
  }

  public function getRefund() {
    $refund = [
      'refund_type_id' => $this->submission['refund_type'],
      'refund_status_id' => $this->submission['refund_status'],
      'amount' => (isset($this->submission['refund_amount']) ? floatval($this->submission['refund_amount']) * 100 : 0),
    ];
    return $refund;
  }

  protected function getGametype() {
    $gametype_id = (isset($this->submission['gametype_id']) && $this->submission['gametype_id'] ? $this->submission['gametype_id'] : 1);
    if ($this->try_other_gametype) {
      $gametype_id = ($gametype_id == 1 ? 2 : 1);
    }
    return $gametype_id;
  }

  public function getAddress() {
    $address = [
      'postcode' => (!$this->postcode_as_note && isset($this->submission['postcode']) && $this->submission['postcode'] ? $this->submission['postcode'] : '0000'),
      'country_id' => $this->getCountryID($this->submission['country']),
    ];

    if (!$this->street_as_note && isset($this->submission['street']) && $this->submission['street']) {
      $address['street'] = $this->submission['street'];
    }

    if (isset($this->submission['town']) && $this->submission['town']) {
      $address['town'] = $this->submission['town'];
    }

    if (isset($this->submission['suburb']) && $this->submission['suburb']) {
      $address['locality'] = $this->submission['suburb'];
    }

    if (isset($this->submission['state']) && $this->submission['state']) {
      $address['region'] = $this->submission['state'];
    }

    return $address;
  }

  public function getInteraction($customer_id = null) {
    $notes = [
      'Form: '.$this->submission['form_title'],
    ];

    $notes_system = [
      'system' => 'Form Importer',
      'submission' => [
        'id' => $this->submission['id'],
      ],
      'form' => [
        'id' => $this->form['id'],
        'name' => $this->form['name'],
      ],
    ];

    if (isset($this->submission['ip']) && $this->submission['ip']) {
      $notes[] = 'Customer IP: '.$this->submission['ip'];
      $notes_system['submission']['ip'] = $this->submission['ip'];
    }

    if (isset($this->submission['source_url']) && $this->submission['source_url']) {
      $notes[] = 'URL: '.$this->submission['source_url'];
    }

    if (isset($this->submission['players']) && $this->submission['players']) {
       $notes[] = 'Players: '. $this->submission['players'];
    }

    if ($this->phone_as_note) {
      if (isset($this->submission['phone_primary']) && $this->submission['phone_primary']) {
        $notes[] = 'Phone (Primary): '.$this->submission['phone_primary'];
      }
      if (isset($this->submission['phone_secondary']) && $this->submission['phone_secondary']) {
        $notes[] = 'Phone (Secondary): '.$this->submission['phone_secondary'];
      }
    }

    if ($this->street_as_note && isset($this->submission['street']) && $this->submission['street']) {
        $notes[] = 'Street Address: '.$this->submission['street'];
    }

    if ($this->email_as_note && isset($this->submission['email']) && $this->submission['email']) {
        $notes[] = 'Email: '.$this->submission['email'];
    }

    if ($this->game_date_as_note && isset($this->submission['game_date']) && $this->submission['game_date']) {
        $notes[] = 'Game Date: '.$this->submission['game_date'];
    }

    if ($this->postcode_as_note && isset($this->submission['postcode']) && $this->submission['postcode']) {
        $notes[] = 'Postcode: '.$this->submission['postcode'];
    }

    if ($this->ticket_as_note && isset($this->submission['tracking']) && $this->submission['tracking']) {
      $notes[] = 'Tracking Code: '.$this->submission['tracking'];
    }

    if (isset($this->submission['notes']) && $this->submission['notes'] && is_array($this->submission['notes'])) {
      foreach ($this->submission['notes'] as $field => $note) {
        if ($note) {
          $notes[] = $field.': '. $note;
        }
      }
    }

    if (isset($this->submission['message']) && $this->submission['message']) {
      $notes[] = $this->submission['message'];
    }

    $interaction = [
      'interaction_channel_id' => 7,      //online form
      'direction_id' => 1,
      'notes_user' => implode(PHP_EOL, $notes),
      'notes_system' => json_encode($notes_system),
    ];

    if ($this->tags) {
      $interaction['tags'] = array_map(function($tag) {
        return $tag['id'];
      }, $this->tags);
    }

    return $interaction;
  }

  public function setCustomTag($name, $category_id) {
    $hapi_tag = $this->getTag($name, $category_id);
    if ($hapi_tag) {
      foreach ($this->tags as $i => $tag) {
        if ($tag['tag_category_id'] == $hapi_tag['tag_category_id']) {
          $this->tags[$i] = $hapi_tag;
          return $hapi_tag;
        }
      }
      $this->tags[] = $hapi_tag;
      return $hapi_tag;
    }
    return false;
  }

  public function handleValidationResponse($ex) {
    $json_response = json_decode($ex->getMessage(), true);
    if ($json_response === null || !isset($json_response['errors'])) {
      throw $ex;
    }

    if (isset($json_response['errors']['phones.0.value']) || isset($json_response['errors']['phones.1.value'])) {
      unset($json_response['errors']['phones.0.value'], $json_response['errors']['phones.1.value']);
      if ($this->getPhoneAsNote()) {
        throw $ex;
      }
      error_log('#'.$this->submission['id'].': Moving Phone To Notes');
      $this->setPhoneAsNote(true);
    }

    if (isset($json_response['errors']['emails.0.value'])) {
      unset($json_response['errors']['emails.0.value']);
      if ($this->getEmailAsNote()) {
        throw $ex;
      }
      error_log('#'.$this->submission['id'].': Moving Email To Notes');
      $this->setEmailAsNote(true);
    }

    if (isset($json_response['errors']['address.street'])) {
      unset($json_response['errors']['address.street']);
      if ($this->getStreetAsNote()) {
        throw $ex;
      }
      error_log('#'.$this->submission['id'].': Moving Street To Notes');
      $this->setStreetAsNote(true);
    }

    if (isset($json_response['errors']['address.postcode'])) {
      unset($json_response['errors']['address.postcode']);
      if ($this->getPostcodeAsNote()) {
        throw $ex;
      }
      error_log('#'.$this->submission['id'].': Moving Postcode To Notes');
      $this->setPostcodeAsNote(true);
    }

    if (isset($json_response['errors']['bookings.0.gametype_id'])) {
      unset($json_response['errors']['bookings.0.gametype_id']);
      if ($this->getTryOtherGametype()) {
        throw $ex;
      }
      error_log('#'.$this->submission['id'].': Trying Other Game Type');
      $this->setTryOtherGametype(true);
    }

    if (isset($json_response['errors']['bookings.0.game_date'])) {
      unset($json_response['errors']['bookings.0.game_date']);
      if ($this->getGameDateAsNote()) {
        throw $ex;
      }
      error_log('#'.$this->submission['id'].': Moving Game Date To Notes');
      $this->setGameDateAsNote(true);
    }

    if (isset($json_response['errors']['name_first']) && isset($json_response['errors']['name_last'])) {
      unset($json_response['errors']['name_first'], $json_response['errors']['name_last']);
      if ($this->getUseUnknownName()) {
        throw $ex;
      }
      error_log('#'.$this->submission['id'].': Using "Unknown" Name');
      $this->setUseUnknownName(true);
    }

    $get_ticket_as_note = $this->getTicketAsNote();
    foreach ($json_response['errors'] as $error => $details) {
      if (substr($error, 11, 8) == 'tickets.' && substr($error, -11) == '.ticket_raw') {    //11 to skip bookings.0
        unset($json_response['errors'][$error]);
        if ($get_ticket_as_note) {
          throw $ex;
        }
        error_log('#'.$this->submission['id'].': Moving Ticket To Notes');
        $this->setTicketAsNote(true);
      }
    }

    if (sizeof($json_response['errors'])) {
      throw $ex;
    }
    return true;
  }
}
